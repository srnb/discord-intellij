package tf.bug.discordintellij.Slack;

public static class SlackAPIKeys {
#error You need to set up your API keys.
    // This file is excluded from GitHub. You must update this file to build.
    public static String SLACK_OAUTH_CLIENT_ID = "<omitted>";
    public static String SLACK_OAUTH_CLIENT_SECRET = "<omitted>";
}
