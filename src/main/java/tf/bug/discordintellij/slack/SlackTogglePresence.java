package tf.bug.discordintellij.slack;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import tf.bug.discordintellij.PresenceActive;
import tf.bug.discordintellij.PresenceAgent;
import tf.bug.discordintellij.TogglePresence;

public class SlackTogglePresence extends TogglePresence {

    @Override
    public PresenceActive getActive(Project project) {
        return ServiceManager.getService(project, SlackActive.class).getPresenceActive();
    }

    @Override
    public Class<? extends PresenceAgent> getAgentClass() {
        return SlackAgent.class;
    }
}
